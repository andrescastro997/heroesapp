import { Component, OnInit } from '@angular/core';
import { Heroe, Publisher } from '../../interfaces/heroes.interface';
import { HeroesService } from '../../services/heroes.service';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmarComponent } from '../../components/confirmar/confirmar.component';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styles: [`
  img{
    width: 100%;
    border-radius: 5px
  }
  `
  ]
})
export class AgregarComponent implements OnInit {
  publishers = [
    {
      id: 'DC comics',
      desc: 'dc-comics'
    },
    {
      id: 'marvel comics',
      desc: 'marvel-comics'
    }
  ];
  heroe: Heroe = {
    superhero: '',
    alter_ego: '',
    characters: '',
    first_appearance: '',
    publisher: Publisher.DCComics,
    alt_img: ''
  }
  constructor( 
    private heroesService: HeroesService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    if(!this.router.url.includes('editar')){
      return
    }
    this.activatedRoute.params
    .pipe(
      switchMap(({id}) => this.heroesService.obtenerHeroe(id))
    )
      .subscribe(heroe =>this.heroe = heroe);
  }

  guardar(){
    console.log(this.heroe);
    if(this.heroe.superhero.trim().length === 0){
      return;
    }

    if(this.heroe.id){
      //update
      this.heroesService.actualizarHeroe(this.heroe)
        .subscribe(heroe => this.mostrarSnackBar('Registro actualizado'))
    }else{
      //create
      this.heroesService.agregarHeroe(this.heroe)
      .subscribe(heroe =>{
        console.log('Respuesta: ', heroe);
        this.router.navigate(['/heroes/editar', heroe.id]);
        this.mostrarSnackBar('Regsitro creado')
      })
    }


  }

  borrarHeroe(){
   const dialog = this.dialog.open(ConfirmarComponent, {
      width: '500px',
      data: this.heroe
    });
    dialog.afterClosed().subscribe(
      (result =>{
        console.log(result);
        if(result){
          this.heroesService.borrarHeroe(this.heroe.id!)
            .subscribe(resp =>{
              this.router.navigate(['/heroes'])
              this.mostrarSnackBar('Se ha eliminado el registro!')
            });
        }
      })
    )

  }

  mostrarSnackBar(mensaje: string): void{
    this.snackBar.open(mensaje, 'x',{
      duration: 2500
    });
  }

}
